///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern int printCat(const int index);

extern int printAllCats();

extern int findCat(const char name[]);

extern char* colorName(const enum Color color);

extern char* genderName(const enum Gender gender);

extern char* breedName(const enum Breed breed);
