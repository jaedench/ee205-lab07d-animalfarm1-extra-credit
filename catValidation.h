///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file catValidation.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern bool nameExists( const int index, const char checkName[] );

extern bool nameEmpty( const int index );

extern bool isValidIndex( const int index );

extern bool isValidName( const char checkName[] );

extern bool isValidWeight( const float checkWeight );
