///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file config.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <limits.h>

#define PROGRAM_TITLE "Animal Farm 1"
#define PROGRAM_NAME "animalfarm1"

#define MAX_CATS (1024)
