///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h> 
#include <stdio.h>

#include "catDatabase.h"

extern bool addCat(const char name[], 
                  const enum Gender gender, 
                  const enum Breed breed, 
                  const bool isFixed, 
                  const float weight,
                  const enum Color collarColor1,
                  const enum Color collarColor2,
                  const unsigned long long license);
